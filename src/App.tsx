import React from 'react';
import './App.css';

import AddFoodTrendForm from './components/AddFoodTrendForm/AddFoodTrendForm';
import AllFoodTrends from './components/AllFoodTrends/AllFoodTrends';

function App() {
  return (
    <div className="App">
      <AddFoodTrendForm />
      <AllFoodTrends />
    </div>
  );
}

export default App;
