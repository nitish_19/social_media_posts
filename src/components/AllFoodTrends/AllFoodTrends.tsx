import React, { useEffect, useState } from 'react';
import { getData } from './../../services/NodeServices';
import { FoodTrend } from './../../API';
import { SERVER_URL } from './../../services/NodeServices';
import './AllFoodTrends.css';

const AllFoodTrends = () => {
  const [foodTrends, setFoodTrends] = useState<FoodTrend[]>([]);

  const fetchAllFoodTrends = async () => {
    const foodTrends: FoodTrend[] = await getData('foodtrend/all');
    console.log('food', foodTrends);
    setFoodTrends(foodTrends);
  }

  useEffect(() => {
    fetchAllFoodTrends();
  }, []);

  return (
    <>
      <h1>Food Trends</h1>
      <ul>
        {foodTrends.map((foodtrend: FoodTrend) => (
          <li>
             <div className="food-trend">
              {foodtrend.title} <br/>
              {foodtrend.description}    
              <img 
                src={`${SERVER_URL}/foodtrend/image/${foodtrend.image}`}
                alt="food-trend"
              />
            </div>
          </li>
        ))}
      </ul>
  </>
  )
}

export default AllFoodTrends;