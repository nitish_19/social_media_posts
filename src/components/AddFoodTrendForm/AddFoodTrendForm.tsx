import React, { FormEvent, useState, ChangeEvent } from 'react';
import './AddFoodTrendForm.css';
import { postDataAndImage, postData } from './../../services/NodeServices';
import { SaveFoodTrendBody } from '../../API';

const App = () => {

  const [title, setTitle] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [foodTrendImage, setFoodTrendImage] = useState<File>();
  const [foodTrendImageName, setFoodTrendImageName] = useState<string>('./no-image.png');
  const [saveFoodTrendBtn, setSaveFoodTrendBtn] = useState<boolean>(false);

  const handleFoodTrendImageChange = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files[0];
    setFoodTrendImage(file);
    setFoodTrendImageName(file.name);
    setSaveFoodTrendBtn(true);
  }

  const handleFoodTrendImageUpload = async () => {
    const formData = new FormData();
    formData.append('image', foodTrendImage);

    const config = { headers: { "content-type": "multipart/form-data" } };
    const response = await postDataAndImage('foodtrend/image-upload', formData, config);
    if(response){
      setSaveFoodTrendBtn(false);
    }
  }

  const handleFoodTrendSubmit = async () => {
    const data: SaveFoodTrendBody = {
      title: title,
      description: description,
      image: foodTrendImageName,
    }

    const response = await postData('foodtrend/create', data);
    
    if(response) {
      alert('food trend added');
    } else {
      alert('Failed');
    }
  }

  return (
    <div className="root-container">
      <div className="styled-formwrapper">
        <div className="styled-form">

          <h2>Food - Trends</h2>
         
          <input
            type="text"
            name="title"
            size={49}
            value={title}
            placeholder= "Title"
            onChange={e => setTitle(e.target.value)}   
          />
          <br/>
         
          <textarea
            name="Description"
            value={description}
            rows={10}
            cols={50}
            placeholder= "Description"
            style={{ marginTop: "5px"}}
            onChange={e => setDescription(e.target.value)} 
          />
          
          <div style = {{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            marginTop: "5px",
            justifyContent: "space-around"
          }}>
            <input
              type="file"
              accept="image/*"
              onChange = {handleFoodTrendImageChange}
              />  
            {saveFoodTrendBtn ? <button onClick={()=>handleFoodTrendImageUpload()}>Save</button>: <></>}
          </div>
          
          <button
            type='submit'
            className="styled-button"
            onClick={handleFoodTrendSubmit}
          >
            Submit
          </button>
          
        </div>
      </div>
    </div>
  );
}

export default App;