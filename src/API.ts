export interface SaveFoodTrendBody {
  title: string,
  description: string,
  image: string
}

export interface FoodTrend {
  title: string,
  description: string,
  image: string
}