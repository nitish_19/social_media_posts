import axios from 'axios';
import * as dotenv from 'dotenv';
import { SaveFoodTrendBody } from './../API';
dotenv.config();

export const SERVER_URL = process.env.SERVER_URL || 'http://localhost:5000';

// service to post data and Image to backend server
export const postDataAndImage = async (URL: string, formData: FormData, config: any) => {
  try {
      const response = await axios.post(`${SERVER_URL}/${URL}`, formData, config);
      return response;
  } catch (e) {
      return null;
  }
};

// service to post only data to backend server
export const postData = async (URL: string, body: SaveFoodTrendBody) => {
  try {
      const response = await fetch(`${SERVER_URL}/${URL}`, {
          method: "POST",
          mode: "cors",
          headers: { "Content-Type": "application/json;charset=utf-8" },
          body: JSON.stringify(body),
      });
      const result = await response.json();
      return result;
  } catch (e) {
      return null;
  }
};

// service to post only data to backend server
export const getData = async (URL: string) => {
    try {
        const response = await fetch(`${SERVER_URL}/${URL}`, {
            method: "GET",
            mode: "cors",
            headers: { "Content-Type": "application/json;charset=utf-8" },
        });
        const result = await response.json();
        return result.data;
    } catch (e) {
        return null;
    }
  };